class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end


  def get_move
    puts "Where would you like to move?"
    gets.chomp.split(", ").map(&:to_i)
  end

  def display(board)
    row0 = ""
    (0..2).each do |col|
      row0 << (board.empty?([0, col]) ? " " : board.grid[[0][col]].to_s)
    end
    row1 = ""
    (0..2).each do |col|
      row1 << (board.empty?([1, col]) ? " " : board.grid[[1][col]].to_s)
    end
    row2 = ""
    (0..2).each do |col|
      row2 << (board.empty?([2, col]) ? " " : board.grid[[2][col]].to_s)
    end

    puts row0
    puts row1
    puts row2
  end
end
