class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end


  def display(board)
    @board = board
  end

  def get_move
    moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos = [row, col]
        moves << pos if board.grid[row][col] == nil
      end
    end

    moves.each do |move|
      return move if winning_move?(move)
    end

    return move.sample
  end

  def winning_move?(move)
    #p board, move
    board.grid[move.first][move.last] = mark
    #p board
    if board.winner == mark
      return true
    else
      board.grid[move.first][move.last] = nil
      return false
    end
  end

end
