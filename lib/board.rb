class Board
  attr_accessor :grid

  def self.starting_board
    Array.new(3) { Array.new(3) }
  end


  def initialize(grid = Board.starting_board)
    @grid = grid
    @marks = [:X, :O]
  end

  def place_mark(pos, mark)
  #takes a position and a mark. Throws error if the position isnt empty
  grid[pos.first][pos.last] = mark
  end

  def empty?(pos)
    # which takes a position as an argument
    collumn = pos.first
    row = pos.last
    grid[collumn][row] == nil
  end

  def winner
    if rows
      return rows
    elsif collumn
      return collumn
    elsif l_diagonal
      return l_diagonal
    else
      return r_diagonal
    end
  end

  def rows
    same = grid.select {|row| row.first}.flatten
    return same[0] if same[0] == same[1] && same[0] == same[2]
  end

  def collumn
    count = 0
    while count < 3
      same = []
      grid.each {|col| same << col[count]}
      if same[0] == same[1] && same[0] == same[2] && same[0] != nil
        return same[0]
      else
        same = []
        count += 1
      end
    end
  end

  def l_diagonal
    same = []
    grid.select.with_index {|col, col_i| same << col[col_i]}
    return same[0] if same[0] == same[1] && same[0] == same[2] && same[0] != nil
  end

  def r_diagonal
    same = []
    grid.reverse.select.with_index {|col, col_i| same << col[col_i]}
    return same[0] if same[0] == same[1] && same[0] == same[2] && same[0] != nil
  end

  def over?
    return true if no_moves? || winner
  end

  def no_moves?
    grid.all? do |col|
      col.none? {|row| row == nil}
    end
  end

end
