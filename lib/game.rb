require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :current_player, :player1, :player2, :board

  def initialize(player1, player2)
      @player1 = player1
      @player2 = player2
      @board = Board.new
      @current_player = player1
      player1.mark = :X
      player2.mark = :O
  end

  def switch_players!
    self.current_player = current_player == player1 ? player2 : player1
  end

  def play_turn
  # which handles the logic for a single turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def play
    until over?
      play_turn
    end

    # which calls play_turn each time through a loop until the game is over
  end
end
